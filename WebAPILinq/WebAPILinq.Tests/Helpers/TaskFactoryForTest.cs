using System;
using System.Collections.Generic;
using DAL.Models;

namespace WebAPILinq.Tests.Helpers
{
    public static class TaskFactoryForTest
    {
        public static List<TaskModel> CreateTasks()
        {
            return new List<TaskModel>
            {
                new TaskModel
                {
                    Id = 1,
                    Name = "Quasi consectetur nesciunt doloribus.",
                    Description = "Sed voluptas quia dolores expedita eius laborum ut qui aspernatur.\n" +
                                  "Molestias sapiente pariatur fuga architecto sed.\n" +
                                  "Autem repellendus maxime magni qui exercitationem rerum.\n" +
                                  "Dolorem magnam aut commodi nemo aut quaerat.\n" +
                                  "Eos sit veniam qui molestiae facere voluptatem.\n" +
                                  "Facilis eum atque enim dolor facilis ea ipsum tempora.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T22:22:09.9030937+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-12-10T22:30:51.0724501+00:00"),
                    TaskStateId = 1,
                    ProjectId = 1,
                    PerformerId = 1,
                },
                new TaskModel()
                {
                    Id = 2,
                    Name = "Praesentium ut consequatur cumque eveniet suscipit amet officia.",
                    Description = "Praesentium autem consequatur magnam et doloribus exercitationem.\n" +
                                  "Aut animi fuga cupiditate debitis atque nisi consequatur consequatur.\n" +
                                  "Cupiditate necessitatibus quo eos sequi earum et quis accusamus.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T11:49:39.729857+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-07-19T22:05:30.3390708+00:00"),
                    TaskStateId = 2,
                    ProjectId = 2,
                    PerformerId = 1
                },
                new TaskModel()
                {
                    Id = 3,
                    Name = "Error sit sunt.",
                    Description = "Unde dignissimos libero minima quas aliquam.\n" +
                                  "Consequuntur aliquid non.\n" +
                                  "Eligendi quia quidem nihil sit veritatis.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T22:52:33.0645959+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-07-25T07:38:48.6570938+00:00"),
                    TaskStateId = 2,
                    ProjectId = 1,
                    PerformerId = 3
                },
                new TaskModel()
                {
                    Id = 4,
                    Name = "Repellendus itaque expedita est ut.",
                    Description = "Nisi esse accusamus dolorem blanditiis porro est dolores.\n" +
                                  "Explicabo consequatur rem dignissimos odit praesentium.\n" +
                                  "Molestiae facilis et tenetur.\n" +
                                  "Voluptas quis sed et ab nulla omnis cupiditate.\n" +
                                  "Id sed et.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T01:35:10.4670552+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-10-19T05:59:38.2813374+00:00"),
                    TaskStateId = 3,
                    ProjectId = 2,
                    PerformerId = 3
                },
                new TaskModel()
                {
                    Id = 5,
                    Name = "Voluptate neque vel molestiae dolor nulla voluptas voluptas optio.",
                    Description = "Rerum totam sit.\n" +
                                  "Velit saepe iusto et repellat et consequuntur sit.\n" +
                                  "Voluptate officiis pariatur ut ea.\n" +
                                  "Neque ut sed voluptatem occaecati.\n" +
                                  "Dolor velit quaerat molestiae assumenda veritatis voluptatem.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T19:51:23.2733812+00:00"),
                    FinishedAt = Convert.ToDateTime("2021-06-20T02:07:03.7230486+00:00"),
                    TaskStateId = 4,
                    ProjectId = 1,
                    PerformerId = 2
                },
                new TaskModel()
                {
                    Id = 6,
                    Name = "In qui labore omnis voluptas voluptates aspernatur consectetur pariatur.",
                    Description = "Sit tenetur nihil laborum qui quia assumenda ratione.\n" +
                                  "Tempora esse deleniti quia debitis incidunt odio consequatur unde.\n" +
                                  "Et quos quam consectetur excepturi sint qui enim autem eaque.\n" +
                                  "Adipisci praesentium officia non quod vel rerum nihil.\n" +
                                  "Veniam qui incidunt dolorum.\n" +
                                  "Molestiae ea officia qui explicabo nulla repellat.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T06:47:59.8929665+00:00"),
                    FinishedAt = Convert.ToDateTime("2020-10-26T16:26:22.2573206+00:00"),
                    TaskStateId = 2,
                    ProjectId = 2,
                    PerformerId = 3
                }
            };
        }
    }
}