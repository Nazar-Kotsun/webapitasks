using Microsoft.Extensions.DependencyInjection;
using BLL.Services;
using BLL.Services.Interfaces;
using DAL.UnitOfWork;


namespace WebAPILinq.Extentions
{
    public static class ServiceExtention
    {
        public static void AddProjectServices(this IServiceCollection services)
        {
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IPositionService, PositionService>();
            services.AddScoped<ITaskStateService, TaskStateService>();
            
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}