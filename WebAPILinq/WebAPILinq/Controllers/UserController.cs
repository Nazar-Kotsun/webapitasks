using System;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;


namespace WebAPILinq.Controllers
{
    
    [ApiController]
    [Route("api/")]
    public class UserController : Controller
    {
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Route("users")]
        public async Task<IActionResult> GetAll()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            var result = await _userService.GetAllUsers();
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        [HttpGet]
        [Route("users/withSortedTasks")]
        public async Task<IActionResult> GetUsersWithSortedTasks()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            var result = await _userService.GetUsersWithSortedTasks();
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpGet]
        [Route("users/detailedInfo/{userId}")]
        public async Task<IActionResult> GetUsersDetailedInfo(int userId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            var result = await _userService.GetDetailedInformByUserId(userId);
            if (result != null)
            { 
                return Ok(result);
            }
            else
            { 
                return NotFound("This user doesn't exist or this user doesn't have any projects");
            }
        }

        [HttpPost]
        [Route("users")]
        public async Task<IActionResult> AddUser([FromBody] UserModel user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _userService.AddUser(user);
            return Created("", user);
        }
        
        [HttpDelete]
        [Route("users/{userId}")]
        public async Task<IActionResult> Delete(int userId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _userService.DeleteUser(userId);
            return Ok("User was deleted");
        }

        [HttpPut]
        [Route("users")]
        public async Task<IActionResult> Update(UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _userService.UpdateUser(userModel);
            return Ok("User was updated");
        }
        
    }
}