using System;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebAPILinq.Controllers
{
    [ApiController]
    [Route("api/")]
    public class TeamController : Controller
    {
        private readonly ITeamService _teamService;

        public TeamController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        [Route("teams")]
        public async Task<IActionResult> GetAll()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            var result = await _teamService.GetAllTeams();
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpGet]
        [Route("teams/withUsers")]
        public async Task<IActionResult> GetTeamsWithUsers()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            var result = await _teamService.GetTeamsWithUsers();
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpPost]
        [Route("teams")]
        public async Task<IActionResult> AddTeam([FromBody] TeamModel teamModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _teamService.AddTeam(teamModel);
            return Created("", teamModel);
            
        }
        
        [HttpDelete]
        [Route("teams/{teamId}")]
        public async Task<IActionResult> DeleteProject(int teamId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _teamService.DeleteTeam(teamId);
            return Ok("Team was deleted");
        }
        
        [HttpPut]
        [Route("teams")]
        public async Task<IActionResult> UpdateProject(TeamModel teamModel)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _teamService.UpdateTeam(teamModel);
            return Ok("Team was updated");
        }
        
    }
}