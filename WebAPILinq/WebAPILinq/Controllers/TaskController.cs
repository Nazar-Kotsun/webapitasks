using System;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;


namespace WebAPILinq.Controllers
{
    [ApiController]
    [Route("api/")]
    public class TaskController : Controller
    {

        private readonly ITaskService _taskService;

        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }
        
        [HttpGet]
        [Route("tasks")]
        public async Task<IActionResult>  GetAll()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            var result = await _taskService.GetAllTasks();
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("tasks/byUserID/{userId}")]
        public async Task<IActionResult> GetTasksByUserId(int userId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            var result = await _taskService.GetTasksByUserId(userId);
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpGet]
        [Route("tasks/finishedInCurrentYear/{userId}")]
        public async Task<IActionResult> GetTasksFinishedInCurrentYear(int userId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            var result = await _taskService.GetTasksFinishedInCurrentYear(userId);
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("tasks/notComplete/{userId}")]
        public async Task<IActionResult> GetNotCompleteTasksByUserId(int userId)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            var result = await _taskService.GetNotCompleteTasksByUserId(userId);
            if (result.Any())
            {
                return Ok(result);
            }

            return BadRequest("No user was found");
        }
        
        [HttpPost]
        [Route("tasks")]
        public async Task<IActionResult> AddTask([FromBody] TaskModel taskModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _taskService.AddTask(taskModel);
            return Created("", "Task was created!");
        }
        
        [HttpDelete]
        [Route("tasks/{taskId}")]
        public async Task<IActionResult> DeleteTask(int taskId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _taskService.DeleteTask(taskId);
            return Ok("Task was deleted");
        }
        
        [HttpPut]
        [Route("tasks")]
        public async Task<IActionResult> UpdateProject(TaskModel taskModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _taskService.UpdateTask(taskModel);
            return Ok("Task was updated");
        }
        
    }
}