using System;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;


namespace WebAPILinq.Controllers
{
    [ApiController]
    [Route("api/")]
    public class PositionController : Controller
    {
        
        private readonly IPositionService _positionService;

        public PositionController(IPositionService positionService)
        {
            _positionService = positionService;
        }

        [HttpGet]
        [Route("positions")]
        public async Task<IActionResult> GetAll()
        {
            var result = await _positionService.GetAllPositions();
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpPost]
        [Route("positions")]
        public async Task<IActionResult> AddPosition([FromBody] PositionModel positionModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _positionService.AddPosition(positionModel);
            return Created("", "Position was created!");
        }
        
        [HttpDelete]
        [Route("positions/{positionID}")]
        public async Task<IActionResult> DeletePosition(int positionID)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _positionService.DeletePosition(positionID);
            return Ok("Position was deleted");
        }

        [HttpPut]
        [Route("positions")]
        public async Task<IActionResult> UpdatePosition(PositionModel positionModel)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _positionService.UpdatePosition(positionModel);
            return Ok("Position was updated");
        }
        
    }
}