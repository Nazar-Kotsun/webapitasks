using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using DAL.Models;

//using Newtonsoft.Json;
using Xunit;

namespace WebAPILinq.Integration.Tests
{
    public class ProjectControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private HttpClient _client;
        private readonly CustomWebApplicationFactory<Startup> _factory;
        
        public ProjectControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }
        
        public void Dispose() { }

        [Fact]
        public async Task AddProject_ThanResponseWithCodeCreated()
        {
            _client = _factory.CreateClient();
            
            ProjectModel newProject = new ProjectModel
            {
                Id = 11,
                Name = "Aspernatur vero quas et ipsum.",
                Description = "Qui rem mollitia inventore nulla nam nam excepturi.\n" +
                                  "Quibusdam distinctio iste quo dolor.\n" +
                                  "Beatae consequatur qui est quo amet et quia.",
                CreatedAt = Convert.ToDateTime("2020-07-01T04:41:59.5052577+00:00"),
                Deadline = Convert.ToDateTime("2021-05-04T02:01:44.6833676+00:00"),
                AuthorId = 3,
                TeamId = 2,
                Price = 56000
            };
            
            string strSerializeProject = JsonSerializer.Serialize<ProjectModel>(newProject);

            HttpContent httpContent = new StringContent(strSerializeProject, Encoding.UTF8, "application/json");
            var responseMessage = await _client.PostAsync("api/projects", httpContent);

            string strResponse = await responseMessage.Content.ReadAsStringAsync();
            var createdProject = JsonSerializer.Deserialize<ProjectModel>(strResponse);
            
            Assert.Equal(HttpStatusCode.Created, responseMessage.StatusCode);
            Assert.NotNull(createdProject);
            Assert.Equal(newProject.Id, createdProject.Id);
        }
        
        [Fact]
        public async Task AddProject_WhenProjectAlreadyExist_ThanResponseWithCodeBadRequest()
        {
            _client = _factory.CreateClient();
            ProjectModel newProject = new ProjectModel
            {
                Id = 1,
                Name = "Aspernatur vero quas et ipsum.",
                Description = "Qui rem mollitia inventore nulla nam nam excepturi.\n" +
                              "Quibusdam distinctio iste quo dolor.\n" +
                              "Beatae consequatur qui est quo amet et quia.",
                CreatedAt = Convert.ToDateTime("2020-07-01T04:41:59.5052577+00:00"),
                Deadline = Convert.ToDateTime("2021-05-04T02:01:44.6833676+00:00"),
                AuthorId = 3,
                TeamId = 2,
                Price = 56000
            };
            
            string strSerializeProject = JsonSerializer.Serialize<ProjectModel>(newProject);

            HttpContent httpContent = new StringContent(strSerializeProject, Encoding.UTF8, "application/json");
            var responseMessage = await _client.PostAsync("api/projects", httpContent);
            
            Assert.Equal(HttpStatusCode.BadRequest, responseMessage.StatusCode);
        }
    }
}