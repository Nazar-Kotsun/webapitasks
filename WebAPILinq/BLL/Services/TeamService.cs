using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Services.Interfaces;
using DAL.Models;
using DAL.UnitOfWork;

namespace BLL.Services
{
    public class TeamService: ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TeamService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable<TeamModel>> GetAllTeams()
        {
            return await _unitOfWork.TeamRepository.GetAll();
        }
        public async Task<IEnumerable<TeamUsersDTO>> GetTeamsWithUsers()
        {
            var teamsModels = await GetAllTeams();
            var usersModels = await _unitOfWork.UserRepository.GetAll();
            
            var result = teamsModels
                .GroupJoin(usersModels.OrderByDescending(u => u.RegisteredAt),
                    team => team.Id,
                    user => user.TeamId,
                    (team, user) => new TeamUsersDTO()
                    {
                        TeamId = team.Id,
                        TeamName = team.Name,
                        ListUsers = user
                    }
                ).Where(team => team.ListUsers
                    .All(user => DateTime.Now.Year - user.Birthday.Year > 10));
            
            return result;
        }

        public async Task AddTeam(TeamModel teamModel)
        {
            if (teamModel != null)
            {
                
                await _unitOfWork.TeamRepository.Create(teamModel);
                await _unitOfWork.SaveChanges();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task DeleteTeam(int teamId)
        {
            if (await _unitOfWork.TeamRepository.GetById(teamId) == null)
            {
                throw new InvalidOperationException("This team doesn't exist");
            }
            
            
            await _unitOfWork.TeamRepository.Delete(teamId);
            await _unitOfWork.SaveChanges();
        }

        public async Task UpdateTeam(TeamModel teamModel)
        {
            if (teamModel != null)
            {
                if ( await _unitOfWork.TeamRepository.GetById(teamModel.Id) == null)
                { 
                    throw new InvalidOperationException("This team doesn't exist");
                }
                
                await _unitOfWork.TeamRepository.Update(teamModel);
                await _unitOfWork.SaveChanges();
            }

            else
            {
                throw new NullReferenceException();
            }
        }
    }
}