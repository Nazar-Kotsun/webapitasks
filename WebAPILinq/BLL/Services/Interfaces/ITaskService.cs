using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTO;
using DAL.Models;


namespace BLL.Services.Interfaces
{
    public interface ITaskService
    {
        Task<IEnumerable<TaskModel>> GetAllTasks();
        Task<IEnumerable<TaskModel>> GetTasksByUserId(int userId);
        Task<IEnumerable<TaskDTO>> GetTasksFinishedInCurrentYear(int userId);
        Task AddTask(TaskModel taskModel);
        Task DeleteTask(int taskId);
        Task UpdateTask(TaskModel taskModel);
        Task<IEnumerable<TaskModel>> GetNotCompleteTasksByUserId(int userId);

    }
}