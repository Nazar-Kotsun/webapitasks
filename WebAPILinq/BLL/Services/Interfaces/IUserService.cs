using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Models;
using BLL.DTO;

namespace BLL.Services.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserModel>> GetAllUsers();
        Task<IEnumerable<UserTasksDTO>> GetUsersWithSortedTasks();
        Task<UserDetailedDTO> GetDetailedInformByUserId(int userId);
        Task AddUser(UserModel user);
        Task DeleteUser(int userId);
        Task UpdateUser(UserModel userModel);
    }
}