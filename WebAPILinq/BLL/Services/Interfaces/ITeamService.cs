using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTO;
using DAL.Models;

namespace BLL.Services.Interfaces
{
    public interface ITeamService
    {
        Task<IEnumerable<TeamModel>> GetAllTeams();
        Task<IEnumerable<TeamUsersDTO>> GetTeamsWithUsers();
        Task AddTeam(TeamModel teamModel);
        Task DeleteTeam(int teamId);
        Task UpdateTeam(TeamModel teamModel);
    }
}