using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Services.Interfaces;
using DAL.Models;
using DAL.UnitOfWork;


namespace BLL.Services
{
    public class TaskStateService : ITaskStateService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TaskStateService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        public async Task<IEnumerable<TaskStateModel>> GetAllTaskStates()
        {
           return await _unitOfWork.TaskStateRepository.GetAll();
        }

        public async Task AddTaskState(TaskStateModel taskStateModel)
        {
            if (taskStateModel != null)
            {
                if (await _unitOfWork.TaskStateRepository.GetById(taskStateModel.Id) != null)
                {
                    throw new InvalidOperationException("This state already exists");
                }
                
                await _unitOfWork.TaskStateRepository.Create(taskStateModel);
                await _unitOfWork.SaveChanges();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task DeleteTaskState(int taskStateId)
        {
            if (await _unitOfWork.TaskStateRepository.GetById(taskStateId) == null)
            {
                throw new InvalidOperationException("This state doesn't exist");
            }
            
            await _unitOfWork.TaskStateRepository.Delete(taskStateId);
            await _unitOfWork.SaveChanges();;
        }

        public async Task UpdateTaskState(TaskStateModel taskStateModel)
        {
            if (taskStateModel != null)
            {
                await _unitOfWork.TaskStateRepository.Update(taskStateModel);
                await _unitOfWork.SaveChanges();
            }

            else
            {
                throw new NullReferenceException();
            }
        }
    }
}