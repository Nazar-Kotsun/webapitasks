using DAL.Models;

namespace BLL.DTO
{
    public class UserDetailedDTO
    {
        public UserModel User { get; set; }
        public ProjectModel LastProject { get; set; }
        public int CountOfTasks { get; set; }
        public int CountOfStartedOrCanceledTasks { get; set; }
        public TaskModel TheLongestTask { get; set; }
    }
}