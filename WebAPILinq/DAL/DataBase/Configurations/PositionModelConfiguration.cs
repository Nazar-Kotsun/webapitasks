using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DAL.ModelBuilderExtension;

namespace DAL.DataBase.Configurations
{
    public class PositionModelConfiguration : IEntityTypeConfiguration<PositionModel>
    {
        public void Configure(EntityTypeBuilder<PositionModel> builder)
        {
            builder.Property(p => p.Name).IsRequired().HasMaxLength(200);

            builder.HasData(PositionModelFactory.CreatePositions());
        }
    }
}