using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DAL.ModelBuilderExtension;

namespace DAL.DataBase.Configurations
{
    public class TaskStateConfiguration : IEntityTypeConfiguration<TaskStateModel>
    {
        public void Configure(EntityTypeBuilder<TaskStateModel> builder)
        {
            builder.Property(t => t.Value).IsRequired().HasMaxLength(100);

            builder.HasData(TaskStateModelFactory.CreateTaskStates());
        }
    }
    
}