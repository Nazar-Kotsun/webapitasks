using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DAL.ModelBuilderExtension;

namespace DAL.DataBase.Configurations
{
    
    public class ProjectConfiguration : IEntityTypeConfiguration<ProjectModel>
    {
        public void Configure(EntityTypeBuilder<ProjectModel> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Name).HasMaxLength(1000);
            builder.Property(p => p.Description).HasMaxLength(4000);
            builder.Property(p => p.CreatedAt).IsRequired();
            builder.Property(p => p.Deadline).IsRequired();
            builder.Property(p => p.AuthorId).IsRequired();
            builder.Property(p => p.TeamId).IsRequired();
            builder.Property(p => p.Price).HasColumnType("money").IsRequired();
            
            builder.HasData(ProjectModelFactory.CreateProjects());
        }
    }
}