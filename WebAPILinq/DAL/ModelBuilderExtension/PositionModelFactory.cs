using System.Collections.Generic;
using DAL.Models;


namespace DAL.ModelBuilderExtension
{
    public static class PositionModelFactory
    {
        public static List<PositionModel> CreatePositions()
        {
            return new List<PositionModel>
            {
                new PositionModel
                {
                    Id = 1,
                    Name = "QC/QA Engineer"
                },
                new PositionModel
                {
                    Id = 2,
                    Name = "Project Manager" 
                },
                new PositionModel
                {
                    Id = 3,
                    Name = "Developer"
                },
                new PositionModel
                {
                    Id = 4,
                    Name = "Administrator"
                },
                new PositionModel
                {
                    Id = 5,
                    Name = "Architect"
                },
            };
        }
    }
}