using System.Threading.Tasks;
using DAL.Models;
using DAL.Repositories;


namespace DAL.UnitOfWork
{
    public interface IUnitOfWork
    {
        IBaseRepository<TaskModel> TaskRepository { get; }
        IBaseRepository<ProjectModel> ProjectRepository { get; }
        IBaseRepository<UserModel> UserRepository { get; }
        IBaseRepository<TeamModel> TeamRepository { get; }
        IBaseRepository<PositionModel> PositionRepository { get; }
        IBaseRepository<TaskStateModel> TaskStateRepository { get; }
        Task<int> SaveChanges();
    }
}