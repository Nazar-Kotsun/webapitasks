using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;


namespace DAL.Models
{
    public class TaskModel
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("description")]
        public string Description { get; set; }
        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }
        [JsonPropertyName("finishedAt")]
        public DateTime FinishedAt { get; set; }
        
        [JsonPropertyName("projectId")]
        [ForeignKey("FK_project")]
        public int ProjectId { get; set; }
        
        [ForeignKey("FK_User")]
        [JsonPropertyName("performerId")]
        public int PerformerId { get; set; }
        
        [ForeignKey("FK_TaskState")]
        [JsonPropertyName("taskStateId")]
        public int TaskStateId { get; set; }
        
        [JsonIgnore]
        public ProjectModel Project { get; set; }
        
        [JsonIgnore]
        public UserModel Performer { get; set; }
        
        [JsonIgnore]
        public TaskStateModel TaskState { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"Description: {Description}\n" +
                   $"StateId: {TaskStateId}\n" +
                   $"CreatedAt: {CreatedAt}\n" +
                   $"FinishedAt: {FinishedAt}\n" +
                   $"ProjectId: {ProjectId}\n" +
                   $"PerformerId: {PerformerId}";
        }
    }
}
