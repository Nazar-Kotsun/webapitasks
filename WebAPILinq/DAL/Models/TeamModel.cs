using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;


namespace DAL.Models
{
    public class TeamModel
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonIgnore]
        public List<UserModel> Users { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"CreatedAt: {CreatedAt}";
        }
    }
}