using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LInqClient.DTO;
using LInqClient.HttpLinqClient;
using LInqClient.Models;
using LInqClient.Models.DTO;
using System.Timers;



namespace LInqClient.Menu
{
    public class MainMenu
    {
        private LinqClientRequest _lInqClient;

        private bool exit = false;
        private int choice;
        
        public MainMenu()
        {
            _lInqClient = new LinqClientRequest();
        }
        private async Task DrawSeparatorLine(string text)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("------------" + text + "------------");
            });
        }
        private async Task StopProgramToContinue()
        {
            await Task.Run(() =>
            {
                Console.Write("Please enter any key to continue...");
                Console.ReadKey();
                Console.Clear();
            });
        }
        
        private Task CheckDate(ref DateTime firstDate, ref DateTime secondDate)
        {
            TaskCompletionSource<object> tcs = new TaskCompletionSource<object>();
            try
            { 
                while (firstDate > secondDate || firstDate == secondDate)
                { 
                    Console.WriteLine("First date must be less than second");
                    Console.Write("First date: ");
                    firstDate = Convert.ToDateTime(Console.ReadLine());
                    Console.Write("Second date: "); 
                    secondDate = Convert.ToDateTime(Console.ReadLine());
                }
                tcs.SetResult("Success");
            }
            catch (FormatException ex)
            { 
                tcs.SetException(ex);
            }
            catch (Exception ex)
            {
                tcs.SetException(ex);
            }

            return tcs.Task;
        }
        
        public async Task StartMenu()
        {
            while (!exit)
            {
               await ShowMenu();
            }
        }
        private async Task ShowMenu()
        {
            Console.WriteLine("1 - All the requests to models");
            Console.WriteLine("2 - User CRUD");
            Console.WriteLine("3 - Project CRUD");
            Console.WriteLine("4 - Task CRUD");
            Console.WriteLine("5 - Team CRUD");
            Console.WriteLine("6 - Task state CRUD");
            Console.WriteLine("7 - Position CRUD");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("8 - Delayed task");
            Console.ResetColor();
            Console.WriteLine("0 - Exit");
            await DoChoice();
        }
        
        private Task<int> CheckCorrectEnteringInteger()
        {
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            int value;
            while (!int.TryParse(Console.ReadLine(), out value))
            {
                Console.WriteLine("Incorrect entering!");
            }
            tcs.SetResult(value);
            return tcs.Task;
        }
        
        private async Task DoChoice()
        {
            Console.Write("Your choice: ");
            choice = await CheckCorrectEnteringInteger();
            
            switch (choice)
            {
                case 1:
                {
                    await RequestsToModels();
                    break;
                }
                case 2:
                {
                    await UserCRUD();
                    break;
                }
                case 3:
                {
                    await ProjectCRUD();
                    break;
                }
                case 4:
                {
                    await TaskCRUD();
                    break;
                }
                case 5:
                {
                    await TeamCRUD();
                    break;
                }
                case 6:
                {
                    await TaskStateCRUD();
                    break;
                }
                case 7:
                {
                    await PositionCRUD();
                    break;
                }
                case 8:
                {
                     RunDelayedTask();
                     break;
                }
                case 0:
                {
                    await Exit();
                    break;
                }
            }
        }
        
        private async Task RequestsToModels()
        {
            Console.WriteLine("1 - Get dictionary(key - project; value - count of tasks) -> (task_1)");
            Console.WriteLine("2 - Get the list of tasks where name is less than 45 characters -> (task_2)");
            Console.WriteLine("3 - Get the list (id,name) of the tasks that ended in 2020 -> (task_3)");
            Console.WriteLine("4 - Get the list (id, name of team, list users) of the teams whose members\n" +
                              "are older than 10 year -> (task_4)");
            Console.WriteLine("5 - Get the list of users alphabetically with sorted tasks by length \n "
                              +"  of name -> (task_5)");
            Console.WriteLine("6 - Get more detailed information about user -> (task_6)");
            Console.WriteLine("7 - Get more detailed information about project -> (task_7)");
            Console.WriteLine("8 - Get not complete tasks by user id");
            
            Console.Write("Your choice: ");
            choice = await CheckCorrectEnteringInteger();

            switch (choice)
            {
                case 1:
                {
                    await GetProjectsWithTasks();
                    await StopProgramToContinue();
                    break;
                }
                case 2:
                {
                    await GetTasksByUserId();
                    await StopProgramToContinue();
                    break;
                }
                case 3:
                {
                    await GetTasksFinishedInCurrentYear();
                    await StopProgramToContinue();
                    break;
                }
                case 4:
                {
                    await GetTeamsWithUsers();
                    await StopProgramToContinue();
                    break;
                }
                case 5:
                {
                    await GetUsersWithSortedTasks();
                    await StopProgramToContinue();
                    break;
                }
                case 6:
                {
                    await GetUserDetailedDTO();
                    await StopProgramToContinue();
                    break;
                }
                case 7:
                {
                    await GetProjectsDetailedInfo();
                    await StopProgramToContinue();
                    break;
                }
                case 8:
                {
                    await GetNotCompleteTasks();
                    await StopProgramToContinue();
                    break;
                }
            }
        }
        
        private async void RunDelayedTask()
        {
            int delay;
            Console.Write("Enter delay: ");
            delay = await CheckCorrectEnteringInteger();
            try
            {
                var taskId = await MarkRandomTaskWithDelay(delay);
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine();
                await DrawSeparatorLine("Task with id - " + taskId + " was finished");
                Console.ResetColor();
            }
            catch (HttpRequestException ex)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ResetColor();
            }
        }  
        
        private async Task<int> MarkRandomTaskWithDelay(int delay)
        {
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            Random rnd = new Random();
            
            Timer timer = new Timer(delay);
            timer.AutoReset = false;
            
            timer.Elapsed += async (sender, args) =>
            {
                try
                {
                    IEnumerable<TaskModel> allTasks = await _lInqClient.GetTasks();

                    List<TaskModel> notFinishedTasks =
                        allTasks.Where(t => t.TaskStateId != (int)TaskState.Finished).ToList();

                    if (notFinishedTasks.Count != 0)
                    {
                        int randomTask = rnd.Next(0, notFinishedTasks.Count - 1);

                        notFinishedTasks[randomTask].TaskStateId = (int)TaskState.Finished;

                        await _lInqClient.UpdateTask(notFinishedTasks[randomTask]);
                        tcs.SetResult(notFinishedTasks[randomTask].Id);


                        timer.Stop();
                        timer.Dispose();
                    }
                    else
                    {
                        tcs.SetException(new InvalidCastException("All the tasks already finished!"));
                    }
                }
                catch (HttpRequestException ex)
                {
                    tcs.SetException(ex);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            };
                
            timer.Start();
            return await tcs.Task;
        }

        private async Task GetProjectsWithTasks()
        {
            try
            {
                int userId;
                Console.Write("Please, enter user id: ");
                userId = await CheckCorrectEnteringInteger();

                IEnumerable<ProjectTasksDTO> projectTasksDtosDtos =
                    await _lInqClient.GetProjectsWithTasksByUserId(userId);

                int i = 1;
                foreach (var project in projectTasksDtosDtos)
                {
                    await DrawSeparatorLine("Project - " + i);
                    Console.WriteLine(project);
                    i++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        private async Task GetTasksByUserId()
        {
            try
            {
                int userId;
                Console.Write("Please, enter user id: ");
                userId = await CheckCorrectEnteringInteger();

                IEnumerable<TaskModel> tasks = await _lInqClient.GetTasksByUSerId(userId);

                int i = 1;
                foreach (var task in tasks)
                {
                    await DrawSeparatorLine("Task - " + i);
                    Console.WriteLine(task);
                    i++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task GetTasksFinishedInCurrentYear()
        {
            try
            {
                int userId;
                Console.Write("Please, enter user id: ");
                userId = await CheckCorrectEnteringInteger();

                IEnumerable<TaskDTO> tasksDtos = await _lInqClient.GetTasksFinishedInCurrentYear(userId);
                
                int i = 1;
                foreach (var task in tasksDtos)
                {
                    await DrawSeparatorLine("Task - " + i);
                    Console.WriteLine(task);
                    i++;
                }
                await DrawSeparatorLine("--------");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task GetTeamsWithUsers()
        {
            try
            {
                IEnumerable<TeamUsersDTO> teamDtos = await _lInqClient.GetTeamsWithUsers();

                int i= 1;
                int j = 1;
                
                foreach (var team in teamDtos)
                {
                    await DrawSeparatorLine("Team - " + i);
                    Console.WriteLine(team);
                    
                    j = 1;
                    foreach (var user in team.ListUsers)
                    {
                        await DrawSeparatorLine("User - " + j);
                        Console.WriteLine(user);
                        j++;
                    }
                    i++;
                }
                await DrawSeparatorLine("-------");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        private async Task GetUsersWithSortedTasks()
        {
            try
            {
                IEnumerable<UserTasksDTO> teamDtos = await _lInqClient.GetUsersWithSortedTasks();

                int i= 1;
                int j = 1;
                
                foreach (var team in teamDtos)
                {
                    await DrawSeparatorLine("User - " + i);
                    Console.WriteLine(team);
                    
                    j = 1;
                    foreach (var task in team.ListTasks)
                    {
                        await DrawSeparatorLine("Task - " + j);
                        Console.WriteLine(task);
                        j++;
                    }
                    i++;
                }
                await DrawSeparatorLine("-------");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task GetUserDetailedDTO()
        {
            try
            {
                int userId;
                Console.Write("Please, enter user id: ");
                userId = await CheckCorrectEnteringInteger();
                
                UserDetailedDTO userInfo = await _lInqClient.GetUsersDetailedInfo(userId);
                
                await DrawSeparatorLine("User");
                Console.WriteLine(userInfo.User);
                await DrawSeparatorLine("LastProject");
                Console.WriteLine(userInfo.LastProject);
                await DrawSeparatorLine("CountOfTasks");
                Console.WriteLine(userInfo.CountOfTasks); 
                await DrawSeparatorLine("CountOfStartedOrCanceledTasks");
                Console.WriteLine(userInfo.CountOfStartedOrCanceledTasks);
                await DrawSeparatorLine("TheLongestTask");
                Console.WriteLine(userInfo.TheLongestTask);
               
                await DrawSeparatorLine("---------");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async Task GetProjectsDetailedInfo()
        {
            try
            {
                IEnumerable<ProjectDetailedDTO> projectDetailedDtos =  await _lInqClient.GetProjectsDetailedInfo();
                foreach (var project in projectDetailedDtos)
                {
                    await DrawSeparatorLine("Project");
                    Console.WriteLine(project.Project);
                    await DrawSeparatorLine("The longest task");
                    Console.WriteLine(project.TheLongestTask);
                    await DrawSeparatorLine("The shorted task");
                    Console.WriteLine(project.TheShortedTask);
                    await DrawSeparatorLine("Count of users in team");
                    Console.WriteLine(project.CountOfUsers);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task GetNotCompleteTasks()
        {
            try
            {
                int userId;
                int i = 1;
                Console.Write("Please, enter user id: ");
                userId = await CheckCorrectEnteringInteger();
                
                IEnumerable<TaskModel> tasks = await _lInqClient.GetNotCompleteTasksByUserId(userId);
                
                foreach (var task in tasks)
                { 
                    await DrawSeparatorLine("Task - " + i );
                    Console.WriteLine(task);
                    i++;
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine("This user doesn't exist or dont have any tasks");
            }
        }
        
        private async Task UserCRUD()
        {
            Console.WriteLine("1 - Get all the users");
            Console.WriteLine("2 - Add new user");
            Console.WriteLine("3 - Delete user");
            Console.WriteLine("4 - Update user");
            
            Console.Write("Your choice: ");
            choice = await CheckCorrectEnteringInteger();
            
            switch (choice)
            {
                case 1:
                {
                    await GetAllUsers();
                    await StopProgramToContinue();
                    break;
                }
                case 2:
                {
                    await AddUser();
                    await StopProgramToContinue();
                    break;
                }
                case 3:
                {
                    await DeleteUser();
                    await StopProgramToContinue();
                    break;
                }
                case 4:
                {
                    await UpdateUser();
                    await StopProgramToContinue();
                    break;
                }
                default:
                {
                    await GetAllUsers();
                    break;
                }
            }
        }

        private async Task GetAllUsers()
        {
            int i = 1;
            IEnumerable<User> users =  await _lInqClient.GetUsers();
            foreach (var user in users)
            { 
                await DrawSeparatorLine("User - " + i );
                Console.WriteLine(user);
                i++;
            }
        }
        
        public async Task AddUser()
        {
            try
            {
                User user = new User();
                Console.WriteLine("Please enter new user!");
                // Console.Write("Id: ");
                // user.Id = int.Parse(Console.ReadLine());
                Console.Write("FirstName: ");
                user.FirstName = Console.ReadLine();
                Console.Write("LastName: ");
                user.LastName = Console.ReadLine();
                Console.Write("Email: ");
                user.Email = Console.ReadLine();
                Console.Write("Birthday: ");
                var userBirthday = Convert.ToDateTime(Console.ReadLine());
                Console.Write("RegisteredAt: ");
                var userRegistedAt = Convert.ToDateTime(Console.ReadLine());
                
                await CheckDate(ref userBirthday, ref userRegistedAt);

                user.Birthday = userBirthday;
                user.RegisteredAt = userRegistedAt;
                
                Console.Write("TeamId: ");
                user.TeamId = int.Parse(Console.ReadLine());
                Console.Write("PositionId: ");
                user.PositionId = int.Parse(Console.ReadLine());
                string strMessage = await _lInqClient.AddUser(user);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task DeleteUser()
        {
            int userId;
            Console.Write("Please, enter user id: ");
            userId = await CheckCorrectEnteringInteger();

            string strMessage = await _lInqClient.DeleteUser(userId);
            Console.WriteLine(strMessage);
        }
        
        public async Task UpdateUser()
        {
            try
            {
                User user = new User();
                Console.WriteLine("Please enter user!");
                Console.Write("Id: ");
                user.Id = int.Parse(Console.ReadLine());
                Console.Write("FirstName: ");
                user.FirstName = Console.ReadLine();
                Console.Write("LastName: ");
                user.LastName = Console.ReadLine();
                Console.Write("Email: ");
                user.Email = Console.ReadLine();
                Console.Write("Birthday: ");
                var userBirthday = Convert.ToDateTime(Console.ReadLine());
                Console.Write("RegisteredAt: ");
                var userRegistedAt = Convert.ToDateTime(Console.ReadLine());
                
                await CheckDate(ref userBirthday, ref userRegistedAt);

                user.Birthday = userBirthday;
                user.RegisteredAt = userRegistedAt;
                
                Console.Write("TeamId: ");
                user.TeamId = int.Parse(Console.ReadLine());
                Console.Write("PositionId: ");
                user.PositionId = int.Parse(Console.ReadLine());
                string strMessage = await _lInqClient.UpdateUser(user);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        private async Task ProjectCRUD()
        {
            Console.WriteLine("1 - Get all the projects");
            Console.WriteLine("2 - Add new project");
            Console.WriteLine("3 - Delete project");
            Console.WriteLine("4 - Update project");
            
            Console.Write("Your choice: ");
            choice = await CheckCorrectEnteringInteger();
            
            switch (choice)
            {
                case 1:
                {
                    await GetAllProjects();
                    await StopProgramToContinue();
                    break;
                }
                case 2:
                {
                    await AddProject(); 
                    await StopProgramToContinue();
                    break;
                }
                case 3:
                {
                    await DeleteProject();
                    await StopProgramToContinue();
                    break;
                }
                case 4:
                {
                    await UpdateProject();
                    await StopProgramToContinue();
                    break;
                }
                default:
                {
                    await GetAllProjects();
                    break;
                }
            }
        }

        private async Task GetAllProjects()
        {
            int i = 1;
            IEnumerable<Project> projects =  await _lInqClient.GetProjects();
            foreach (var project in projects)
            { 
                await DrawSeparatorLine("Project - " + i );
                Console.WriteLine(project);
                i++;
            }
        }
        
        public async Task AddProject()
        {
            try
            {
                Project project = new Project();
                Console.WriteLine("Please enter new project!");
                Console.Write("Name: ");
                project.Name = Console.ReadLine();
                Console.Write("Description: ");
                project.Description = Console.ReadLine();
                Console.Write("CreatedAt: ");
                var projectCreatedAt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("Deadline: ");
                var projectDeadline = Convert.ToDateTime(Console.ReadLine());
                
                await CheckDate(ref projectCreatedAt, ref projectDeadline);

                project.CreatedAt = projectCreatedAt;
                project.Deadline = projectDeadline;
                
                Console.Write("AuthorId: ");
                project.AuthorId = int.Parse(Console.ReadLine());
                Console.Write("TeamId: ");
                project.TeamId = int.Parse(Console.ReadLine());
                Console.Write("Price: ");
                project.Price = decimal.Parse(Console.ReadLine());
                
                string strMessage = await _lInqClient.AddProject(project);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public async Task DeleteProject()
        {
            int projectId;
            Console.Write("Please, enter project id: ");
            projectId = await CheckCorrectEnteringInteger();

            string strMessage = await _lInqClient.DeleteProject(projectId);
            Console.WriteLine(strMessage);
        }
        
        public async Task UpdateProject()
        {
            try
            {
                Project project = new Project();
                Console.WriteLine("Please enter project!");
                Console.Write("Id: ");
                project.Id = int.Parse(Console.ReadLine());
                Console.Write("Name: ");
                project.Name = Console.ReadLine();
                Console.Write("Description: ");
                project.Description = Console.ReadLine();
                Console.Write("CreatedAt: ");
                var projectCreatedAt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("Deadline: ");
                var projectDeadline = Convert.ToDateTime(Console.ReadLine());
                
                await CheckDate(ref projectCreatedAt, ref projectDeadline);

                project.CreatedAt = projectCreatedAt;
                project.Deadline = projectDeadline;
                
                Console.Write("AuthorId: ");
                project.AuthorId = int.Parse(Console.ReadLine());
                Console.Write("TeamId: ");
                project.TeamId = int.Parse(Console.ReadLine());
                Console.Write("Price: ");
                project.Price = decimal.Parse(Console.ReadLine());
                
                string strMessage = await _lInqClient.UpdateProject(project);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        private async Task TaskCRUD()
        {
            Console.WriteLine("1 - Get all the tasks");
            Console.WriteLine("2 - Add new task");
            Console.WriteLine("3 - Delete task");
            Console.WriteLine("4 - Update task");
            
            Console.Write("Your choice: ");
            choice = await CheckCorrectEnteringInteger();
            
            switch (choice)
            {
                case 1:
                {
                    await GetAllTasks();
                    await StopProgramToContinue();
                    break;
                }
                case 2:
                {
                    await AddTask();
                    await StopProgramToContinue();
                    break;
                }
                case 3:
                {
                    await DeleteTask();
                    await StopProgramToContinue();
                    break;
                }
                case 4:
                {
                    await UpdateTask();
                    await StopProgramToContinue();
                    break;
                }
                default:
                {
                    await GetAllTasks();
                    break;
                }
            }
        }

        private async Task GetAllTasks()
        {
            int i = 1;
            IEnumerable<TaskModel> tasks =  await _lInqClient.GetTasks();
            foreach (var task in tasks)
            { 
                await DrawSeparatorLine("Task - " + i );
                Console.WriteLine(task);
                i++;
            }
        }
        
        public async Task AddTask()
        {
            try
            {
                TaskModel taskModel = new TaskModel();
                Console.WriteLine("Please enter new task!");
                Console.Write("Name: ");
                taskModel.Name = Console.ReadLine();
                Console.Write("Description: ");
                taskModel.Description = Console.ReadLine();
                Console.Write("CreatedAt: ");
                var taskCreatedAt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("FinishedAt: ");
                var taskFinishedAt = Convert.ToDateTime(Console.ReadLine());
                
                await CheckDate(ref taskCreatedAt, ref taskFinishedAt);

                taskModel.CreatedAt = taskCreatedAt;
                taskModel.FinishedAt = taskFinishedAt;
                
                Console.Write("ProjectId: ");
                taskModel.ProjectId = int.Parse(Console.ReadLine());
                Console.Write("PerformerId: ");
                taskModel.PerformerId = int.Parse(Console.ReadLine());
                Console.Write("TaskStateId: ");
                taskModel.TaskStateId = int.Parse(Console.ReadLine());
                string strMessage = await _lInqClient.AddTask(taskModel);
                Console.WriteLine(strMessage);
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public async Task DeleteTask()
        {
            int taskId;
            Console.Write("Please, enter task id: ");
            taskId = await CheckCorrectEnteringInteger();

            string strMessage = await _lInqClient.DeleteTask(taskId);
            Console.WriteLine(strMessage);
        }
        
        public async Task UpdateTask()
        {
            try
            {
                TaskModel taskModel = new TaskModel();
                Console.WriteLine("Please enter task!");
                Console.Write("Id: ");
                taskModel.Id = int.Parse(Console.ReadLine());
                Console.Write("Name: ");
                taskModel.Name = Console.ReadLine();
                Console.Write("Description: ");
                taskModel.Description = Console.ReadLine();
                Console.Write("CreatedAt: ");
                var taskCreatedAt = Convert.ToDateTime(Console.ReadLine());
                Console.Write("FinishedAt: ");
                var taskFinishedAt = Convert.ToDateTime(Console.ReadLine());
                
                await CheckDate(ref taskCreatedAt, ref taskFinishedAt);

                taskModel.CreatedAt = taskCreatedAt;
                taskModel.FinishedAt = taskFinishedAt;
                
                Console.Write("ProjectId: ");
                taskModel.ProjectId = int.Parse(Console.ReadLine());
                Console.Write("PerformerId: ");
                taskModel.PerformerId = int.Parse(Console.ReadLine());
                Console.Write("TaskStateId: ");
                taskModel.TaskStateId = int.Parse(Console.ReadLine());

                string strMessage = await _lInqClient.UpdateTask(taskModel);
                Console.WriteLine(strMessage);
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        private async Task TeamCRUD()
        {
            Console.WriteLine("1 - Get all the teams");
            Console.WriteLine("2 - Add new team");
            Console.WriteLine("3 - Delete team");
            Console.WriteLine("4 - Update team");
            
            Console.Write("Your choice: ");
            choice = await CheckCorrectEnteringInteger();
            
            switch (choice)
            {
                case 1:
                {
                    await GetAllTeams();
                    await StopProgramToContinue();
                    break;
                }
                case 2:
                {
                    await AddTeam();
                    await StopProgramToContinue();
                    break;
                }
                case 3:
                {
                    await DeleteTeam();
                    await StopProgramToContinue();
                    break;
                }
                case 4:
                {
                    await UpdateTeam();
                    await StopProgramToContinue();
                    break;
                }
                default:
                {
                    await GetAllTeams();
                    break;
                }
            }
        }
        
        private async Task GetAllTeams()
        {
            int i = 1;
            IEnumerable<Team> teams =  await _lInqClient.GetTeams();
            foreach (var team in teams)
            { 
                await DrawSeparatorLine("Team - " + i );
                Console.WriteLine(team);
                i++;
            }
        }

        public async Task AddTeam()
        {
            try
            {
                Team team = new Team();
                Console.WriteLine("Please enter new team!");
                Console.Write("Name: ");
                team.Name = Console.ReadLine();
                Console.Write("CreatedAt: ");
                team.CreatedAt = Convert.ToDateTime(Console.ReadLine());
                
                string strMessage = await _lInqClient.AddTeam(team);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public async Task DeleteTeam()
        {
            int teamId;
            Console.Write("Please, enter team id: ");
            teamId = await CheckCorrectEnteringInteger();

            string strMessage = await _lInqClient.DeleteTeam(teamId);
            Console.WriteLine(strMessage);
        }
        
        public async Task UpdateTeam()
        {
            try
            {
                Team team = new Team();
                Console.WriteLine("Please enter team!");
                Console.Write("Id: ");
                team.Id = int.Parse(Console.ReadLine());
                Console.Write("Name: ");
                team.Name = Console.ReadLine();
                Console.Write("CreatedAt: ");
                team.CreatedAt = Convert.ToDateTime(Console.ReadLine());
                
                string strMessage = await _lInqClient.UpdateTeam(team);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        private async Task TaskStateCRUD()
        {
            Console.WriteLine("1 - Get all the states of task");
            Console.WriteLine("2 - Add new states");
            Console.WriteLine("3 - Delete states");
            Console.WriteLine("4 - Update states");
            
            Console.Write("Your choice: ");
            choice = await CheckCorrectEnteringInteger();
            
            switch (choice)
            {
                case 1:
                {
                    await GetAllTaskStates();
                    await StopProgramToContinue();
                    break;
                }
                case 2:
                {
                    await AddTaskStates();
                    await StopProgramToContinue();
                    break;
                }
                case 3:
                {
                    await DeleteTaskStates();
                    await StopProgramToContinue();
                    break;
                }
                case 4:
                {
                    await UpdateTaskState();
                    await StopProgramToContinue();
                    break;
                }
                default:
                {
                    await GetAllTaskStates();
                    break;
                }
            }
        }
        
        private async Task GetAllTaskStates()
        {
            int i = 1;
            IEnumerable<TaskStateModel> states =  await _lInqClient.GetTasksStates();
            foreach (var state in states)
            { 
                await DrawSeparatorLine("State - " + i );
                Console.WriteLine(state);
                i++;
            }
        }

        public async Task AddTaskStates()
        {
            try
            {
                TaskStateModel taskStateModel = new TaskStateModel();
                Console.WriteLine("Please enter new state of task!");
                Console.Write("Value: ");
                taskStateModel.Value = Console.ReadLine();

                string strMessage = await _lInqClient.AddTaskState(taskStateModel);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public async Task DeleteTaskStates()
        {
            int taskStateId;
            Console.Write("Please, enter state id: ");
            taskStateId = await CheckCorrectEnteringInteger();

            string strMessage = await _lInqClient.DeleteTaskState(taskStateId);
            Console.WriteLine(strMessage);
        }
        
        public async Task UpdateTaskState()
        {
            try
            {
                TaskStateModel taskStateModel = new TaskStateModel();
                Console.WriteLine("Please enter state of task!");
                Console.Write("Id: ");
                taskStateModel.Id = int.Parse(Console.ReadLine());
                Console.Write("Value: ");
                taskStateModel.Value = Console.ReadLine();

                string strMessage = await _lInqClient.UpdateTaskState(taskStateModel);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        private async Task PositionCRUD()
        {
            Console.WriteLine("1 - Get all the position");
            Console.WriteLine("2 - Add new positions");
            Console.WriteLine("3 - Delete positions");
            Console.WriteLine("4 - Update positions");
            
            Console.Write("Your choice: ");
            choice = await CheckCorrectEnteringInteger();
            
            switch (choice)
            {
                case 1:
                {
                    await GetAllPositions();
                    await StopProgramToContinue();
                    break;
                }
                case 2:
                {
                    await AddPosition();
                    await StopProgramToContinue();
                    break;
                }
                case 3:
                {
                    await DeletePosition();
                    await StopProgramToContinue();
                    break;
                }
                case 4:
                {
                    await UpdatePosition();
                    await StopProgramToContinue();
                    break;
                }
                default:
                {
                    await GetAllPositions();
                    break;
                }
            }
        }
        
        private async Task GetAllPositions()
        {
            int i = 1;
            IEnumerable<Position> positions =  await _lInqClient.GetPositions();
            foreach (var position in positions)
            { 
                await DrawSeparatorLine("Position - " + i );
                Console.WriteLine(position);
                i++;
            }
        }
        
        public async Task AddPosition()
        {
            try
            {
                Position positionModel = new Position();
                Console.WriteLine("Please enter new position!");
                Console.Write("Name: ");
                positionModel.Name = Console.ReadLine();

                string strMessage = await _lInqClient.AddPosition(positionModel);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public async Task DeletePosition()
        {
            int positionId;
            Console.Write("Please, enter position id: ");
            positionId = await CheckCorrectEnteringInteger();

            string strMessage = await _lInqClient.DeletePosition(positionId);
            Console.WriteLine(strMessage);
        }
        
        public async Task UpdatePosition()
        {
            try
            {
                Position positionModel = new Position();
                Console.WriteLine("Please enter position!");
                Console.Write("Id: ");
                positionModel.Id = int.Parse(Console.ReadLine());
                Console.Write("Name: ");
                positionModel.Name = Console.ReadLine();

                string strMessage = await _lInqClient.UpdatePosition(positionModel);
                Console.WriteLine(strMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        private async Task Exit()
        {
            await Task.Run(() =>
            {
                exit = true;
            });
        }
    }
}
    