using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models
{
    public class User
    {
        [JsonPropertyName("id")] 
        public int Id { get; set; }
        
        [JsonPropertyName("firstName")] 
        public string FirstName { get; set; }
        
        [JsonPropertyName("lastName")] 
        public string LastName { get; set; }
        
        [JsonPropertyName("email")] 
        public string Email { get; set; }
        
        [JsonPropertyName("birthday")] 
        public DateTime Birthday { get; set; }
        
        [JsonPropertyName("registeredAt")] 
        public DateTime RegisteredAt { get; set; }
        
        [JsonPropertyName("teamId")] 
        public int? TeamId { get; set; }
        
        [JsonPropertyName("positionId")] 
        public int PositionId { get; set; }

        // public Team UserTeams { get; set; }
        
        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"FirstName: {FirstName}\n" +
                   $"LastName: {LastName}\n" +
                   $"Email: {Email}\n" +
                   $"Birthday: {Birthday}\n" +
                   $"RegisteredAt: {RegisteredAt}\n" +
                   $"TeamId: {TeamId}\n" +
                   $"PositionId: {PositionId}";
        }
        
    }
}