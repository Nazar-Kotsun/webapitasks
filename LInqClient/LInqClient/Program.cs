﻿using LInqClient.Menu;

namespace LInqClient
{
    class Program
    {
        public static void Main(string[] args)
        {
            MainMenu menu = new MainMenu();
            menu.StartMenu().Wait();
        }
    }
}